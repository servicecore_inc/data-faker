<?php

namespace ServiceCore\DataFaker\ServiceManager\Factory;

use Assetic\Cache\ArrayCache;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\DataFaker\ServiceManager\DataFaker as FakerSM;

class DataFaker implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): FakerSM
    {
        $config   = $container->get('config');
        $sm       = new FakerSM();
        $cache    = new ArrayCache();
        $services = \array_merge(
            [
                'config'                  => $config,
                'faker.cache'             => $cache,
                EntityManager::class      => $container->get(EntityManager::class),
                ContainerInterface::class => $container
            ],
            $config['faker']['services'] ?? []
        );

        $sm->configure(
            [
                'abstract_factories' => $config['faker']['service_manager']['abstract_factories'] ?? [],
                'factories'          => $config['faker']['service_manager']['factories'] ?? [],
                'services'           => $services
            ]
        );

        return $sm;
    }
}
