<?php

namespace ServiceCore\DataFaker\ServiceManager;

use Laminas\ServiceManager\ServiceManager;
use ServiceCore\DataFaker\Context\AbstractDataProvider;

class DataFaker extends ServiceManager
{
    public function getFakerInvokables(): array
    {
        $services = [];
        $cache    = $this->get('faker.cache');

        foreach ($this->services as $service) {
            if (\is_subclass_of($service, AbstractDataProvider::class)) {
                $class = $this->build($service, ['cache' => $cache]);

                $services[$service] = $class;
            }
        }

        return $this->buildDependencyTree($services);
    }

    private function buildDependencyTree(array $services): array
    {
        $instantiatedServices = [];
        $instantiatedEntities = [];
        $dependencies         = [];

        foreach ($services as $name => $service) {
            $dependencies[$name] = $service;
        }

        while (\count($dependencies) > 0) {
            /** @var AbstractDataProvider $service*/
            foreach ($dependencies as $name => $service) {
                $unsatisfiedDeps = null;
                if ($service->getEntityDependencies() === []) {
                    $instantiatedEntities[]      = $service->getEntityClassName();
                    $instantiatedServices[$name] = $service;

                    unset($dependencies[$name]);
                } else {
                    $unsatisfiedDeps = \array_diff($service->getEntityDependencies(), $instantiatedEntities);

                    if ($unsatisfiedDeps === []) {
                        $instantiatedEntities[]      = $service->getEntityClassName();
                        $instantiatedServices[$name] = $service;

                        unset($dependencies[$name]);
                    }
                }
            }
        }

        return \array_values($instantiatedServices);
    }
}
