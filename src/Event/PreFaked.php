<?php

namespace ServiceCore\DataFaker\Event;

use Laminas\EventManager\Event;
use Laminas\Stdlib\ParametersInterface;
use ServiceCore\DataFaker\Context\AbstractDataProvider;

class PreFaked extends Event
{
    public function __construct(AbstractDataProvider $target, ?ParametersInterface $params = null)
    {
        parent::__construct(
            self::class,
            $target,
            $params
        );
    }
}
