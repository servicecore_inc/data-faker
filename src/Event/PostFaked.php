<?php

namespace ServiceCore\DataFaker\Event;

use Laminas\EventManager\Event;
use Laminas\Stdlib\ParametersInterface;
use ServiceCore\DataFaker\Context\AbstractDataProvider;

class PostFaked extends Event
{
    private bool $shouldStopFaker = false;

    public function __construct(AbstractDataProvider $target, ?ParametersInterface $params = null)
    {
        parent::__construct(
            self::class,
            $target,
            $params
        );
    }

    public function shouldStopFaker(): bool
    {
        return $this->shouldStopFaker;
    }

    public function setShouldStopFaker(bool $shouldStopFaker): self
    {
        $this->shouldStopFaker = $shouldStopFaker;

        return $this;
    }
}
