<?php

namespace ServiceCore\DataFaker\Context\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\EventManager\EventManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\DataFaker\Context\DataFaker as DataFakerContext;
use ServiceCore\DataFaker\ServiceManager\DataFaker as DataFakerSM;

class DataFaker implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): DataFakerContext
    {
        /** @var DataFakerSM $sm */
        $sm = $container->get(DataFakerSM::class);

        /** @var EntityManager $em */
        $em = $container->get(EntityManager::class);

        return new DataFakerContext($sm, new EventManager(), $em);
    }
}
