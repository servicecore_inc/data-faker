<?php

namespace ServiceCore\DataFaker\Context\Factory;

use DirectoryIterator;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use League\FactoryMuffin\FactoryMuffin;
use League\FactoryMuffin\Stores\RepositoryStore;
use RuntimeException;
use ServiceCore\DataFaker\Context\AbstractDataProvider as AbstractDataProviderContext;

class AbstractDataProvider implements AbstractFactoryInterface
{
    protected ?FactoryMuffin $factoryMuffin = null;

    public function canCreate(ContainerInterface $container, $requestedName): bool
    {
        if (\is_subclass_of($requestedName, AbstractDataProviderContext::class)) {
            return true;
        }

        return false;
    }

    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): AbstractDataProviderContext {
        if (!\array_key_exists('cache', $options)) {
            throw new RuntimeException('Faker cache can not be found.');
        }

        /** @var EntityManager $em */
        $em = $container->get(EntityManager::class);

        if (!$this->factoryMuffin) {
            $this->includeAssembliesAndFactories($em);
        }

        return new $requestedName($options['cache'], $em, $this->factoryMuffin);
    }

    protected function includeAssembliesAndFactories(EntityManager $entityManager): void
    {
        $em = $entityManager;

        // create a new data-factory (must be named "fm")
        $fm = new FactoryMuffin(new RepositoryStore($em));

        // set the factory's and assembly's root path
        $root = \realpath(BASE_PATH . '/tests/_support');

        // load the factories
        $fm->loadFactories(\realpath($root . '/factories'));

        // loop through the assemblies
        foreach (new DirectoryIterator(\realpath($root . '/assemblies')) as $item) {
            // if the item is a file
            if ($item->isFile()) {
                // include it bb! (requires $em and $fm to be defined)
                include($item->getPathname());
            }
        }

        $this->factoryMuffin = $fm;
    }
}
