<?php

namespace ServiceCore\DataFaker\Context;

use Doctrine\ORM\EntityManagerInterface;
use Laminas\EventManager\EventManagerAwareInterface;
use Laminas\EventManager\EventManagerAwareTrait;
use Laminas\EventManager\EventManagerInterface;
use Laminas\Stdlib\ParametersInterface;
use ServiceCore\DataFaker\Event\PostFaked;
use ServiceCore\DataFaker\Event\PreFaked;
use ServiceCore\DataFaker\ServiceManager\DataFaker as FakerSM;

class DataFaker implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    private FakerSM                $serviceManager;
    private EntityManagerInterface $entityManager;

    public function __construct(
        FakerSM $serviceManager,
        EventManagerInterface $eventManager,
        EntityManagerInterface $entityManager
    ) {
        $this->serviceManager = $serviceManager;
        $this->entityManager  = $entityManager;

        $this->setEventManager($eventManager);
    }

    public function __invoke(ParametersInterface $parameters): void
    {
        $this->entityManager->getConnection()->exec("USE {$parameters->get('dbName')}");

        /**
         * @var AbstractDataProvider $invokable
         */
        foreach ($this->serviceManager->getFakerInvokables() as $invokable) {
            $this->events->trigger(PreFaked::class, new PreFaked($invokable, $parameters));

            $invokable($parameters);

            $postFaked = new PostFaked($invokable, $parameters);

            $this->events->trigger(PostFaked::class, $postFaked);

            if ($postFaked->shouldStopFaker()) {
                break;
            }
        }
    }
}
