<?php

namespace ServiceCore\DataFaker\Context;

use Assetic\Cache\CacheInterface as EntityCache;
use Doctrine\ORM\EntityManagerInterface;
use Laminas\Stdlib\ParametersInterface;
use League\FactoryMuffin\FactoryMuffin;

abstract class AbstractDataProvider
{
    protected EntityCache $cache;

    protected EntityManagerInterface $em;

    protected FactoryMuffin $factory;

    abstract public function __invoke(ParametersInterface $parameters): void;

    abstract public function getEntityClassName(): string;

    abstract public function getEntityDependencies(): array;

    abstract public function getOutputLabel(): string;

    public function __construct(
        EntityCache $cache,
        EntityManagerInterface $entityManager,
        FactoryMuffin $factory
    ) {
        $this->cache   = $cache;
        $this->em      = $entityManager;
        $this->factory = $factory;
    }

    public function getEntityFromCache(string $name)
    {
        return $this->cache->get($name);
    }

    public function addEntityToCache(string $name, $entity): void
    {
        $this->cache->set($name, $entity);
    }

    public function addEntitiesToCache(string $name, array $entities): void
    {
        $cached = [];
        foreach ($entities as $entity) {
            $cached[\spl_object_hash($entity)] = $entity;
        }

        $this->cache->set($name, $cached);
    }

    protected function batchFlush(int $iteration, ?string $clearableEntity = null, int $flushLimit = 100): void
    {
        if ($iteration % $flushLimit === 0) {
            $this->em->flush();

            if ($clearableEntity) {
                $this->em->clear($clearableEntity);
            }
        }
    }
}
