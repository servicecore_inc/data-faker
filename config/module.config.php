<?php

use ServiceCore\DataFaker\Context\DataFaker as DataFakerContext;
use ServiceCore\DataFaker\Context\Factory\AbstractDataProvider as AbstractDataProviderFactory;
use ServiceCore\DataFaker\Context\Factory\DataFaker as DataFakerContextFactory;
use ServiceCore\DataFaker\ServiceManager\DataFaker as DataFakerSM;
use ServiceCore\DataFaker\ServiceManager\Factory\DataFaker as DataFakerSMFactory;

return [
    'service_manager' => [
        'factories' => [
            DataFakerContext::class => DataFakerContextFactory::class,
            DataFakerSM::class      => DataFakerSMFactory::class
        ]
    ],
    'faker'           => [
        'service_manager' => [
            'abstract_factories' => [
                AbstractDataProviderFactory::class
            ]
        ]
    ]
];
