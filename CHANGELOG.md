# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 3.1.2

### Added

- shouldStopFaker property on the PostFaked event that allows for early breaking
- CHANGELOG.md to better help communicate changes