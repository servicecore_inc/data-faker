<?php

namespace ServiceCore\DataFaker\Test;

use PHPUnit\Framework\TestCase;
use ServiceCore\DataFaker\Module;

class ModuleTest extends TestCase
{
    public function testGetConfig(): void
    {
        $module = new Module();

        $config = $module->getConfig();

        $this->assertArrayHasKey('service_manager', $config);
        $this->assertArrayHasKey('factories', $config['service_manager']);
    }
}
